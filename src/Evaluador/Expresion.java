/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Evaluador;
import ufps.util.colecciones_seed.*;
/**
 *
 * @author estudiante
 */
public class Expresion {
    
  private ListaCD<String> expresiones=new ListaCD();
  private String cadena;
  private static final char sum = '+';
  private static final char res = '-';
  private static final char mul = '*';
  private static final char div = '/';
  private static final char uni = '~';
  

    public Expresion() {
    }
    
    public Expresion(String cadena) {
        
       String v[]=cadena.split(",");
       for(String dato:v) 
           this.expresiones.insertarAlFinal(dato);
    }

    public ListaCD<String> getExpresiones() {
        return expresiones;
    }

    public void setExpresiones(ListaCD<String> expresiones) {
        this.expresiones = expresiones;
    }

    @Override
    public String toString() {
     String msg="";
     for(String dato:this.expresiones)
         //msg+=dato+"<->";
         msg+=dato;
    return msg;
    }
    
    private String[] convertirAvector(String caden) {
        String cad = "";
        String vCad[] = new String[caden.length()];
        int contador = 0;
        for (int i = 0; i < caden.length(); i++) {
            if (caden.charAt(i) != sum && caden.charAt(i) != res
                    && caden.charAt(i) != div && caden.charAt(i) != mul
                    && caden.charAt(i) != '(' && caden.charAt(i) != ')') {
                cad += caden.charAt(i);
            } else {
                if (!cad.equals("")) {
                    vCad[contador] = cad;
                    contador++;
                    cad = "";
                    vCad[contador] = String.valueOf(caden.charAt(i));
                    contador++;
                } else {
                    vCad[contador] = String.valueOf(caden.charAt(i));
                    contador++;
                }
            }
            if (!cad.isEmpty()) {
                vCad[contador] = cad;
            }
        }
        return vCad;
    }
    
    public Cola getPrefijo()
    {
         Cola<String> cola = new Cola();
            String[] c = convertirAvector(cadena);
            Pila<String> p = new <String>Pila();
            for (int i = c.length - 1; i >= 0; i--) {
                if (c[i] != null) {
                    switch (c[i]) {
                        case "+": {
                            if (p.esVacia()) {
                                p.apilar(c[i]);
                            } else if ("-".equals(p.getTope()) || "+".equals(p.getTope())) {
                                cola.enColar(p.desapilar());
                                p.apilar(c[i]);
                            } else if (p.getTope().equals("/") || p.getTope().equals("*")) {
                                while (!p.esVacia()) {
                                    cola.enColar(p.desapilar());
                                }
                                p.apilar(c[i]);
                            } else {
                                p.apilar(c[i]);
                            }
                        }
                        break;
                        case "-": {
                            if (p.esVacia()) {
                                p.apilar(c[i]);
                            } else if ("-".equals(p.getTope()) || "+".equals(p.getTope())) {
                                cola.enColar(p.desapilar());
                                p.apilar(c[i]);
                            } else if ("/".equals(p.getTope()) || "*".equals(p.getTope())) {
                                while (!p.esVacia()) {
                                    cola.enColar(p.desapilar());
                                }
                                p.apilar(c[i]);
                            } else {
                                p.apilar(c[i]);
                            }
                        }
                        break;
                        case "*": {
                            if (p.esVacia()) {
                                p.apilar(c[i]);
                            } else {
                                if ("/".equals(p.getTope()) || "*".equals(p.getTope())) {
                                    cola.enColar(p.desapilar());
                                    p.apilar(c[i]);
                                } else {
                                    p.apilar(c[i]);
                                }
                            }
                        }
                        break;
                        case "/": {
                            if (p.esVacia()) {
                                p.apilar(c[i]);
                            } else {
                                if ("*".equals(p.getTope()) || "/".equals(p.getTope())) {
                                    cola.enColar(p.desapilar());
                                    p.apilar(c[i]);
                                } else {
                                    p.apilar(c[i]);
                                }
                            }
                        }
                        break;
                        case "(": {
                            p.apilar(c[i]);
                        }
                        break;
                        case ")": {
                            while (!p.getTope().equals("(")) {
                                cola.enColar(p.desapilar());
                            }
                            p.desapilar();
                        }
                        break;
                        default:
                            if (c[i].charAt(0) == uni) {
                                cola.enColar(c[i]);
                            } else {
                                cola.enColar(c[i]);
                            }
                    }
                }
            }
            cola.enColar(p.desapilar());
        
        return cola;
    }
    
    
    public Cola getPosfijo()
    {
        Cola<String> cola = new Cola();
            String[] c = convertirAvector(cadena);
            //String[] c = Expresion(cadena);
            Pila<String> p = new <String>Pila();
            for (int i = 0; i < c.length && c[i] != null; i++) {
                switch (c[i]) {
                    case "+": {
                        if (p.esVacia()) {
                            p.apilar(c[i]);
                        } else if ("-".equals(p.getTope()) || "+".equals(p.getTope())) {
                            cola.enColar(p.desapilar());
                            p.apilar(c[i]);
                        } else if (p.getTope().equals("/") || p.getTope().equals("*")) {
                            while (!p.esVacia()) {
                                cola.enColar(p.desapilar());
                            }
                            p.apilar(c[i]);
                        } else {
                            p.apilar(c[i]);
                        }
                    }
                    break;
                    case "-": {
                        if (p.esVacia()) {
                            p.apilar(c[i]);
                        } else if ("-".equals(p.getTope()) || "+".equals(p.getTope())) {
                            cola.enColar(p.desapilar());
                            p.apilar(c[i]);
                        } else if ("/".equals(p.getTope()) || "*".equals(p.getTope())) {
                            while (!p.esVacia()) {
                                cola.enColar(p.desapilar());
                            }
                            p.apilar(c[i]);
                        } else {
                            p.apilar(c[i]);
                        }
                    }
                    break;
                    case "*": {
                        if (p.esVacia()) {
                            p.apilar(c[i]);
                        } else {
                            if ("/".equals(p.getTope()) || "*".equals(p.getTope())) {
                                cola.enColar(p.desapilar());
                                p.apilar(c[i]);
                            } else {
                                p.apilar(c[i]);
                            }
                        }
                    }
                    break;
                    case "/": {
                        if (p.esVacia()) {
                            p.apilar(c[i]);
                        } else {
                            if ("*".equals(p.getTope()) || "/".equals(p.getTope())) {
                                cola.enColar(p.desapilar());
                                p.apilar(c[i]);
                            } else {
                                p.apilar(c[i]);
                            }
                        }
                    }
                    break;
                    case "(": {
                        p.apilar(c[i]);
                    }
                    break;
                    case ")": {
                        while (!p.getTope().equals("(")) {
                            cola.enColar(p.desapilar());
                        }
                        p.desapilar();
                    }
                    break;
                    default:
                        cola.enColar(c[i]);
                }
            }
            if (!p.esVacia()) {
                while (!p.esVacia()) {
                    cola.enColar(p.desapilar());
                }
            }
        return cola;
    }
    
    public String getTotal(){
        String total = "";
        Cola<String> posfija = getPosfijo();
        Pila<Integer> operaciones = new Pila();
        int a, b;
        while (!posfija.esVacia()) {
            switch (posfija.getInfoInicio()) {
                case "+": {
                    b = operaciones.desapilar();
                    a = operaciones.desapilar();
                    operaciones.apilar(getSuma(a, b));
                    posfija.deColar();
                }
                break;
                case "-": {
                    b = operaciones.desapilar();
                    a = operaciones.desapilar();
                    operaciones.apilar(getResta(a, b));
                    posfija.deColar();
                }
                break;
                case "/": {
                    b = operaciones.desapilar();
                    a = operaciones.desapilar();
                    operaciones.apilar(getDivision(a, b));
                    posfija.deColar();
                }
                break;
                case "*": {
                    b = operaciones.desapilar();
                    a = operaciones.desapilar();
                    operaciones.apilar(getMultiplicacion(a, b));
                    posfija.deColar();
                }
                break;
                default:
                    if (posfija.getInfoInicio().charAt(0) == '~') {
                        String s = posfija.getInfoInicio().substring(1, posfija.getInfoInicio().length());
                        operaciones.apilar(Integer.parseInt(s) * -1);
                        posfija.deColar();
                    } else {
                        operaciones.apilar(Integer.parseInt(posfija.deColar()));
                    }
            }
        }
        total = String.valueOf(operaciones.desapilar());
        return total;
    }
    
    public int getSuma(int sumando, int sumando2) {
        return sumando + sumando2;
    }

   
    public int getResta(int minuendo, int sustraendo) {
        return minuendo - sustraendo;
    }

   
    public int getDivision(int dividendo, int divisor) {

        return dividendo / divisor;
    }

    
    public int getMultiplicacion(int multiplicando, int multiplicador) {
        return multiplicando * multiplicador;
    }
    
    public float getEvaluarPosfijo()
    {   
        String valor= getTotal();
        float total= Float.parseFloat(valor);
        return total;
    }
}
